import setuptools

setuptools.setup(
    name="nnunet_for_clouds",
    version="0.0.1",
    author="KP Labs",
    description="Hands-Free Cloud Segmentation in Satellite Images Using nnU-Nets",
    url="https://gitlab.com/jnalepa/nnunets_for_clouds",
    packages=setuptools.find_packages(exclude=[".venv", "tests"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "numpy",
        "SimpleITK",
        "tqdm",
        "nnunet",
        "scikit-image",
    ],
    python_requires="<3.9",
)
