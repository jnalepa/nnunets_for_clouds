"""
Scripts to convert 38-Cloud tiff images to nifti.
Required to use custom tiff datasets with nnU-Net.
"""

import numpy as np
from pathlib import Path
from typing import List, Tuple
from tqdm import tqdm
from nnunet.dataset_conversion.utils import generate_dataset_json

from utils import convert_patch_to_nifti


def convert_38Cloud_to_nifti(
    data_path: Path,
    task_path: Path,
    patches_ids_types: List[Tuple[str, str]],
    spacing: Tuple[float, float, float] = (999., 1., 1.)
):
    """
    Convert 38-Cloud dataset to nifti to enable nnUNet training and evaluation.
    
    :param data_path: path to 38-Cloud dataset.
    :param task_path: path to the task to be created.
    :param patches_ids_types: list of tuples containing patch IDs and
                              patch types ("Tr" for training or "Ts" for testing).
    :param spacing: dataset spacing. Needed to convert dataset to nifti.
    """
    (task_path / 'imagesTr').mkdir(parents=True, exist_ok=False)
    (task_path / 'imagesTs').mkdir(parents=True, exist_ok=False)
    (task_path / 'labelsTr').mkdir(parents=True, exist_ok=False)
    for patch_id, patch_type in tqdm(patches_ids_types):
        if patch_type == 'Tr':
            type_38Cloud = ('training', 'train')
        elif patch_type == 'Ts':
            type_38Cloud = ('test', 'test')
        img_paths = []
        for band in ('red', 'green', 'blue', 'nir'):
            img_paths.append(
                data_path / (f'38-Cloud_{type_38Cloud[0]}/{type_38Cloud[1]}_{band}/' +
                             f'{band}_{patch_id}.TIF')
            )
        gt_path = data_path / (f'38-Cloud_{type_38Cloud[0]}/{type_38Cloud[1]}_gt/' +
                               f'gt_{patch_id}.TIF') if patch_type == 'Tr' else None
        out_path = task_path / f'images{patch_type}/{patch_id}'
        gt_out_path = task_path / f'labels{patch_type}/{patch_id}.nii.gz' \
                      if patch_type == 'Tr' else None
        convert_patch_to_nifti(
            img_files=img_paths,
            lab_file=gt_path,
            img_out_base=out_path,
            anno_out=gt_out_path,
            spacing=spacing
        )
    generate_dataset_json(
        output_file=str(task_path / 'dataset.json'),
        imagesTr_dir=str(task_path / 'imagesTr'),
        imagesTs_dir=str(task_path / 'imagesTs'),
        modalities=('red', 'green', 'blue', 'nir'),
        labels={0: 'non-cloud', 1: 'cloud'},
        dataset_name='38-Cloud',
        license='Apache-2.0 License',
        dataset_description='38-Cloud: A Cloud Segmentation Dataset',
        dataset_reference='https://github.com/SorourMo/38-Cloud-A-Cloud-Segmentation-Dataset',
        dataset_release='0.0'
    )


def generate_patches_ids_types(
    patches_path: Path = Path('datasets/38-Cloud/training_patches_38-cloud_nonempty.csv'),
    patches_type: str = 'Tr'
) -> List[Tuple[str, str]]:
    """
    Generate patches IDs & types tuples from CSV file.

    :param patches_path: path to patches names CSV file.
    :param patches_type: type of generated patches names. "Tr" or "Ts".
    :return: list of patches IDs & types tuples.
    """
    patches_ids = list(
        np.genfromtxt(
            patches_path,
            dtype="str",
            skip_header=1,
        )
    )
    patches_ids_types = []
    for patch_id in patches_ids:
        patches_ids_types.append((patch_id, patches_type))
    return patches_ids_types


if __name__ == "__main__":
    patches_ids_types = (
        generate_patches_ids_types(
            patches_path=Path('datasets/38-Cloud/training_patches_38-cloud_nonempty.csv'),
            patches_type='Tr'
        )
        + generate_patches_ids_types(
            patches_path=Path('datasets/38-Cloud/38-Cloud_test/test_patches_38-Cloud.csv'),
            patches_type='Ts'  
        )
    )
    convert_38Cloud_to_nifti(
        data_path = Path('datasets/38-Cloud/'),
        task_path = Path('nnUNet/raw/nnUNet_raw_data/Task500_38Cloud'),
        patches_ids_types=patches_ids_types
    )
