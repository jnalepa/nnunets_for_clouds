"""
Scripts to convert DD challenge images to nifti.
Required to use this dataset with nnU-Net.
"""

from pathlib import Path
from typing import Tuple
from tqdm import tqdm
from nnunet.dataset_conversion.utils import generate_dataset_json

from utils import convert_patch_to_nifti


def convert_dd_challenge_dataset_to_nifti(
    data_path: Path,
    task_path: Path,
    spacing: Tuple[float] = (999., 1., 1.)
):
    """
    Convert DD challenge dataset to nifti to enable nnUNet training and evaluation.
    
    :param data_path: path to dataset.
    :param task_path: path to the task to be created.
    :param spacing: dataset spacing. Needed to convert dataset to nifti.
    """
    (task_path / 'imagesTr').mkdir(parents=True, exist_ok=False)
    (task_path / 'imagesTs').mkdir(parents=True, exist_ok=False)
    (task_path / 'labelsTr').mkdir(parents=True, exist_ok=False)

    chips_paths = sorted(list(
        [cp for cp in (data_path/'train_features').iterdir() if cp.is_dir()]))

    for chip_path in tqdm(chips_paths):
        img_paths = list(chip_path.glob('*.tif'))
        patch_id = chip_path.stem
        label_path = data_path/f'train_labels/{patch_id}.tif'
        out_path = task_path/f'imagesTr/{patch_id}'
        gt_out_path = task_path/f'labelsTr/{patch_id}.nii.gz'

        convert_patch_to_nifti(
            img_files=sorted(img_paths),
            lab_file=label_path,
            img_out_base=out_path,
            anno_out=gt_out_path,
            spacing=spacing)

    generate_dataset_json(
        output_file=str(task_path/'dataset.json'),
        imagesTr_dir=str(task_path/'imagesTr'),
        imagesTs_dir=None,
        modalities=('B02', 'B03', 'B04', 'B08'),
        labels={0: 'background', 1: 'cloud'},
        dataset_name='dd_challenge_sentinel-2',
    )


if __name__ == '__main__':
    convert_dd_challenge_dataset_to_nifti(
        data_path = Path('datasets/DD-Cloud-Challenge-Sentinel2/'),
        task_path = Path('nnUNet/raw/nnUNet_raw_data/Task501_DD_challenge')
    )
