"""
Utility functions.
"""

from pathlib import Path
from typing import List, Tuple, Optional
from skimage import io
import numpy as np
import SimpleITK as sitk


def convert_patch_to_nifti(
    img_files: List[Path],
    lab_file: Optional[Path],
    img_out_base: Path,
    anno_out: Optional[Path],
    spacing: Tuple[float],
    remove_zeroes: bool = True,
):
    """
    This is a modified version of the function from
    https://github.com/MIC-DKFZ/nnUNet/blob/master/nnunet/dataset_conversion/Task075_Fluo_C3DH_A549_ManAndSim.py.
    The copyright notice from linked file is copied below.

    '''
    Copyright 2020 Division of Medical Image Computing, German Cancer Research Center (DKFZ), Heidelberg, Germany

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    '''

    :param img_files: list of paths to img bands.
    :param lab_file: path to GT; None to ignore.
    :param img_out_base: path to save nifti file.
    :param anno_out: path to save GT.
    :param spacing: spacing param for nifti conversion.
    :param remove_zeroes: whether to ignore imgs with all zeroes.
    """
    assert len(img_files) > 0 and len(img_files) <= 10
    if remove_zeroes:
        zero_count = 0
        for img_file in img_files:
            img = io.imread(img_file)
            img = replace_nans(img)
            if check_if_zero_array(img):
                zero_count += 1
        if zero_count == len(img_files):
            print(f'Removing {img_out_base} consisting of all 0\'s')
            return None
    for i, img_file in enumerate(img_files):
        img = io.imread(img_file)
        img = np.expand_dims(img, axis=0)
        save_as_nifti(
            dir_path=img_out_base.parent,
            fname=img_out_base.stem,
            data=img,
            spacing=spacing,
            band_num=i,
        )
    if lab_file is not None:
        l = io.imread(lab_file)
        l = np.expand_dims(l, axis=0)
        if np.max(l) not in (0, 1):
            l = l/np.max(l)
        assert set(np.unique(l)).issubset((0, 1))
        save_as_nifti(
            dir_path=anno_out.parent,
            fname=anno_out.stem,
            data=l,
            spacing=spacing,
            is_gt=True,
        )


def save_as_nifti(
    dir_path: Path,
    fname: str,
    data: np.ndarray,
    spacing: Tuple[float],
    band_num: int = None,
    is_gt: bool = False,
):
    """
    Save numpy array as nifti.

    :param dir_path: path to output dir.
    :param fname: base file name.
    :param data: data to save as nifti.
    :param spacing: spacing param for nifti conversion.
    :param band_num: band number; used only if is_gt = False.
    :param is_gt: whether the data is a ground truth or img.
    """
    assert not ((not is_gt) and band_num is None)
    data = replace_nans(data)
    if not is_gt:
        data = sitk.GetImageFromArray(data.astype(np.float32))
        data.SetSpacing(np.array(spacing)[::-1])
        sitk.WriteImage(data, str(dir_path / fname) + f'_000{band_num}.nii.gz')
    else:
        data = sitk.GetImageFromArray(data.astype(np.uint8))
        data.SetSpacing(np.array(spacing)[::-1])
        sitk.WriteImage(data, str(dir_path / fname) + '.nii.gz')


def replace_nans(
    data: np.ndarray
) -> np.ndarray:
    """
    Replace NaNs with 0.0.

    :param data: data to replace NaNs.
    :return: data with NaNs replaced.
    """
    if np.isnan(np.sum(data)):
        print(
            'Found NaNs in data:',
            np.count_nonzero(np.isnan(data)),
            '/', data.size,
            f'({(np.count_nonzero(np.isnan(data))/data.size)*100:.5f}%)'
        )
        print('Removing NaNs from data.')
        data = np.nan_to_num(
            x=data,
            copy=True,
            nan=0.0,
            posinf=np.inf,
            neginf=-np.inf
        )        
    return data


def check_if_zero_array(x: np.ndarray) -> bool:
    """
    Check if arrya containts all zeroes.

    :param x: array to check.
    :return: True for all zeroes, False otherwise.
    """
    return np.all(x == 0)
