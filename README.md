# Squeezing Self-Configuring Deep Learning Methods with Knowledge Distillation for On-Board Cloud Detection (under review) #
## Bartosz Grabowski, Maciej Ziaja, Piotr Bosowski, Michal Kawulok, Nicolas Longépé, Bertrand Le Saux, and Jakub Nalepa

This repository contains all supplementary materials for the paper listed above. It includes:

- The detailed nnU-Net architectures elaborated for the Landsat-8 and Sentinel-2 image data for cloud detection.

- Code to transform the Landsat-8 and Sentinel-2 images into the Neuroimaging Informatics Technology Initiative (NIfTI) files required by nnU-Nets.

- Example segmentation maps (together with the csv file presenting the quantitative metrics) elaborated using U-Nets after knowledge distillation (the teachers were nnU-Nets), obtained for the simulated PhiSat-2 imagery.
